﻿using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using CurrencyControl;

namespace CurrencyControl.BL
{
    public interface IFileManager
    {
        bool IsExist(string filePath);
        bool isExistDirectory(string filePath);
        DataTable ImportExceltoDatatable(string file_path, string sheet_name);
        DataTable ImportExceltoDatatable(string file_path, string sheet_name, DateTime start_doc_date, DateTime end_doc_date);
        DataTable GetPrepareData(DataTable dt_nonresident, DataTable dt_saldo);
        DataTable GetTransactionPassportDetect(DataTable dt_source, string PathStatementBankControl);
        DataTable GetCurrencyControlResult(DataTable dt_pass_num, string path);
        DataTable GetParseTransactionPassportExcel(string file_path, string sheet_name);
    }

    public class FileManager: IFileManager
    {
        //Проверка существования файла
        public bool IsExist(string filePath)
        {
            bool isExist = File.Exists(filePath);
            return isExist;
        }

        //Проверка существования папки
        public bool isExistDirectory(string filePath)
        {
            bool isExistDirectory = Directory.Exists(filePath);
            return isExistDirectory;
        }

        //НЕ ИСПОЛЬЗУЕТСЯ. Построчная загрузка Excel в DataTable не используется!
        public DataTable GetImportExcelFile(string file_path, string sheet_name)
        {
            Excel.Application oXl;
            Excel._Workbook oWB;
            Excel._Worksheet oSheet;
            Excel.Range oRng = null;

            oXl = new Excel.Application();

            oXl.Visible = false;
            //делаем временно неактивным документ
            oXl.Interactive = false;
            oXl.EnableEvents = false;

            oWB = oXl.Workbooks.Open(file_path);
            oSheet = (Excel._Worksheet)oXl.Sheets[sheet_name];

            oRng = oSheet.UsedRange;

            DataTable dt = new DataTable();

            for (int i = 1; i <= oRng.Columns.Count; i++)
            {
                dt.Columns.Add(new DataColumn((oRng.Cells[1, i] as Excel.Range).Value2));
            }

            for (int rnum = 2; rnum <= oRng.Rows.Count; rnum++)
            {
                if ((oRng.Cells[rnum, 8] as Excel.Range).Value2 == "DV")
                {
                    DataRow dr = dt.NewRow();
                    for (int cnum = 1; cnum <= oRng.Columns.Count; cnum++)
                    {
                        if ((oRng.Cells[rnum, cnum] as Excel.Range).Value2 != null)
                        {
                            dr[cnum - 1] = (oRng.Cells[rnum, cnum] as Excel.Range).Value2;
                        }
                    }
                    dt.Rows.Add(dr);
                    dt.AcceptChanges();
                } 
            }

            return dt;
        }

        /// <summary>
        /// Импорт данных из Excel в DataGrigView на форму с указанием периода по дате счета
        /// </summary>
        /// <param name="file_path">Путь к Excel файлу</param>
        /// <param name="sheet_name">Название Excel Листа</param>
        /// <param name="start_doc_date">Начальная Дата счета для указания периода</param>
        /// <param name="end_doc_date">Конечная Дата счета для указания периода</param>
        /// <returns></returns>
        public DataTable ImportExceltoDatatable(string file_path, string sheet_name, DateTime start_doc_date, DateTime end_doc_date)
        {
            string start_date = start_doc_date.ToString("MM/dd/yyyy").Replace('.', '/');
            string end_date = end_doc_date.ToString("MM/dd/yyyy").Replace('.', '/');

            string sqlquery = string.Format("Select * From [" + sheet_name + "$] Where type='DV' and [Doc# Date] between #{0}# and #{1}# Order by [Doc# Date]", start_date, end_date);
            DataSet ds = new DataSet();
            string constring = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + file_path + "';Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1\"";
            OleDbConnection con = new OleDbConnection(constring);
            OleDbDataAdapter da = new OleDbDataAdapter(sqlquery, con);
            da.Fill(ds);
            DataTable dt = ds.Tables[0];
            return dt;
        }

        /// <summary>
        /// Импорт данных из Excel в DataGrigView на форму
        /// </summary>
        /// <param name="file_path">Путь к Excel файлу</param>
        /// <param name="sheet_name">Название Excel Листа</param>
        /// <returns></returns>
        public DataTable ImportExceltoDatatable(string file_path, string sheet_name)
        {
            string sqlquery = "";
            switch (sheet_name)
            {
                case "NonResident":
                    sqlquery = string.Format("Select [Account] as [SAP], [Name 1] as [Компания], [Reference] as [Счёт], [Assignment] as [Договор], [Type] as [Тип], [Doc# Date] as [Дата Счёта], SUM([Amount in DC]) as [Сумма], [Curr#] as [Валюта], [Flag] as [Резидентство] From [{0}$] Where type='DV' and Flag in ('MR','NR') Group by [Account], [Name 1], [Reference], [Assignment], [Type], [Doc# Date], [Curr#], [Flag] Order by [Account],[Assignment], [Doc# Date]", sheet_name);
                    break;
                case "trade":
                    sqlquery = string.Format("Select * From [{0}$] where len([Номер Паспорта сделки])=22 Order by [SAP],[Номер договора]", sheet_name);
                    break;
            }
            DataSet ds = new DataSet();
            string constring = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + file_path + "';Extended Properties=\"Excel 12.0;HDR=YES;IMEX=0\"";
            OleDbConnection con = new OleDbConnection(constring);
            OleDbDataAdapter da = new OleDbDataAdapter(sqlquery, con);
            da.Fill(ds);
            DataTable dt = ds.Tables[0];

            #region Попытка Datatable Group by (пока непонятно как результат передавать в DataTable)
            //var result = from datarow in dt.AsEnumerable()
            //             group datarow by new
            //             {
            //                 SAP = datarow[0].ToString(),
            //                 Company = datarow[1].ToString(),
            //                 Reference = datarow[2].ToString(),
            //                 Assignment = datarow[3].ToString(),
            //                 Type = datarow[4].ToString(),
            //                 DocDate = datarow[5].ToString(),
            //                 Currency = datarow[7].ToString(),
            //                 Flag = datarow[8].ToString(),
            //             }
            //             into datarow
            //             select new
            //             {
            //                 SAP = datarow.Key.SAP,
            //                 Company = datarow.Key.Company,
            //                 Reference = datarow.Key.Reference,
            //                 Assignment = datarow.Key.Assignment,
            //                 Type = datarow.Key.Type,
            //                 DocDate = datarow.Key.DocDate,
            //                 TotalAmount = datarow.Sum(x => x.Field<float>("Сумма")),
            //                 Currency = datarow.Key.Currency,
            //                 Flag = datarow.Key.Flag,
            //             };
            #endregion

            return dt;
        }

        //Подготовливаем данные и возвращаем DataTable NonResident + TransactionPassport
        public DataTable GetPrepareData(DataTable dt_nonresident, DataTable dt_saldo)
        {
            DataColumn dc = dt_nonresident.Columns.Add("Паспорт сделки", typeof(string));

            for (int i = 0; i < dt_nonresident.Rows.Count; i++)
            {
                for (int j = 0; j < dt_saldo.Rows.Count; j++)
                {
                    //Условие SAP и №Договора должны совпадать
                    if ((dt_nonresident.Rows[i][0].ToString().Trim() == dt_saldo.Rows[j][4].ToString().Trim()) && (dt_nonresident.Rows[i][3].ToString().Trim() == dt_saldo.Rows[j][2].ToString().Trim()))
                    {
                        dt_nonresident.Rows[i]["Паспорт сделки"] = dt_saldo.Rows[j][0].ToString();
                    }
                }
            }

            #region Обрабатываем данные с помощью LINQ
            var result = from datarow1 in dt_nonresident.AsEnumerable()
                         //where datarow1["pass_num"].ToString() != ""
                         orderby datarow1["Паспорт сделки"].ToString() ascending
                         select datarow1;
            DataTable dt_result = result.CopyToDataTable();
            #endregion

            return dt_result;
        }

        //Поиск паспортов сделки и merge данных с результатом сверки. После возвращает DataTable c данными для отчета 
        public DataTable GetTransactionPassportDetect(DataTable dt_source, string PathStatementBankControl)
        {
            string[] fileEntries = Directory.GetFiles(PathStatementBankControl, "*.*", SearchOption.TopDirectoryOnly);
            //сортируем по возрастанию
            var file_entries_sort = from fe in fileEntries.AsEnumerable()
                                      orderby fe.ToString() ascending
                                      select fe;
            string[] file_entries_array = file_entries_sort.ToArray();

            //исключаем строки без pass_num
            var dt_only_pass_num = from datarow1 in dt_source.AsEnumerable()
                         where datarow1["Паспорт сделки"].ToString() != ""
                         orderby datarow1["Паспорт сделки"].ToString() ascending
                         select datarow1;
            DataTable dt_result = dt_only_pass_num.CopyToDataTable();

            //возвращаем уникальный массив с номерами ПС
            var dt_dist_pass_num = (from datarow1 in dt_result.AsEnumerable()
                        select datarow1["Паспорт сделки"].ToString()).Distinct();
            string[] arr_dist_pass_num = dt_dist_pass_num.ToArray();

            DataTable dt_all_result_cur_control = new DataTable();

            foreach (var arr in arr_dist_pass_num)
            {
                string pass_num = arr.ToString().Replace("/","_");

                foreach (string file_name in file_entries_array)
                {
                    string fname = Path.GetFileNameWithoutExtension(file_name);

                    if (pass_num == fname)
                    {
                        var cc_result = from datarow1 in dt_result.AsEnumerable()
                                        where datarow1["Паспорт сделки"].ToString() == arr.ToString()
                                        select datarow1;
                        DataTable dt_pass_num = cc_result.CopyToDataTable();

                        dt_all_result_cur_control.Merge(GetCurrencyControlResult(dt_pass_num, file_name));
                        break;
                    }
                }
            }

            return dt_all_result_cur_control;
        }

        //Возвращаем данные с результатами сверки
        public DataTable GetCurrencyControlResult(DataTable dt_pass_num, string path)
        {
            DataTable dt_statement_bank_control = GetParseTransactionPassportExcel(path, "sheet");

            DataTable dt_result = new DataTable();

            dt_result.Columns.Add("Паспорт сделки", typeof(string));
            dt_result.Columns.Add("Номер SAP", typeof(string));
            dt_result.Columns.Add("Контрагент", typeof(string));
            dt_result.Columns.Add("Номер Счёта, Платежа", typeof(string));
            dt_result.Columns.Add("Дата Счёта, Платежа", typeof(DateTime));
            dt_result.Columns.Add("Код Валюты, Платежа", typeof(string));
            dt_result.Columns.Add("Сумма, Платежа", typeof(float));
            dt_result.Columns.Add("Номер Счёта, ВБК", typeof(string));
            dt_result.Columns.Add("Дата Счёта, ВБК", typeof(DateTime));
            dt_result.Columns.Add("Код Валюты, ВБК", typeof(string));
            dt_result.Columns.Add("Сумма, ВБК", typeof(float));
            dt_result.Columns.Add("Возмещение услуги", typeof(string));
            dt_result.Columns.Add("Статус", typeof(string));

            foreach (var item_pass_numm in dt_pass_num.AsEnumerable())
            {
                foreach (var item_sbc in dt_statement_bank_control.AsEnumerable())
                {
                    if (item_pass_numm[2].ToString() == item_sbc[1].ToString() && item_pass_numm[5].ToString() == item_sbc[2].ToString())
                    {
                        DataRow dr = dt_result.NewRow();

                        dr[0] = item_pass_numm[9].ToString();
                        dr[1] = item_pass_numm[0].ToString();
                        dr[2] = item_pass_numm[1].ToString();
                        dr[3] = item_pass_numm[2].ToString();
                        dr[4] = item_pass_numm[5].ToString();
                        dr[5] = item_pass_numm[7].ToString();
                        dr[6] = float.Parse(item_pass_numm[6].ToString());
                        dr[7] = item_sbc[1].ToString();
                        dr[8] = item_sbc[2].ToString();
                        dr[9] = item_sbc[3].ToString();
                        dr[10] = float.Parse(item_sbc[4].ToString());
                        dr[11] = float.Parse(item_pass_numm[6].ToString()) - float.Parse(item_sbc[4].ToString());
                        if (float.Parse(item_pass_numm[6].ToString()) - float.Parse(item_sbc[4].ToString()) == 0)
                        {
                            dr[12] = "OK";
                        }

                        dt_result.Rows.Add(dr);
                        dt_result.AcceptChanges();
                    }
                }
            }
            return dt_result;
        }

        //Обрабатываем excel файл паспорта сделки и возвращаем DataTable c данными ВБК "Раздел III. Сведения о подтверждающих документах"
        public DataTable GetParseTransactionPassportExcel(string file_path, string sheet_name)
        {
            string sqlquery = string.Format("Select [F1],[F2],[F11],[F20],[F28],[F46],[F54] From [{0}$]", sheet_name);

            DataSet ds = new DataSet();

            string constring = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + file_path + "';Extended Properties=\"Excel 12.0;HDR=NO;IMEX=1\"";

            OleDbConnection con = new OleDbConnection(constring);
            OleDbDataAdapter da = new OleDbDataAdapter(sqlquery, con);
            da.Fill(ds);

            DataTable dt = ds.Tables[0];
            DataTable dt_result = new DataTable();

            dt_result.Columns.Add("П/п", typeof(string));
            dt_result.Columns.Add("Номер счёта", typeof(string));
            dt_result.Columns.Add("Дата счёта", typeof(DateTime));
            dt_result.Columns.Add("Код валюты, Платежа", typeof(string));
            dt_result.Columns.Add("Сумма, Платежа", typeof(float));
            dt_result.Columns.Add("Код валюты, Контракта", typeof(string));
            dt_result.Columns.Add("Сумма, Контракта", typeof(float));

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].ToString() == "Раздел III. Сведения о подтверждающих документах")
                {
                    int index_row = dt.Rows.IndexOf(dt.Rows[i]) + 7;

                    while ((dt.Rows[index_row][0].ToString() != "Подраздел III.II Сведения о подтверждающих документах (справочно)") && (dt.Rows[index_row][0].ToString() != ""))
                    {
                        DataRow dr = dt_result.NewRow();
                        
                        dr[0] = dt.Rows[index_row][0].ToString();
                        try
                        {
                            dr[1] = Convert.ToDouble(dt.Rows[index_row][1].ToString());
                        }
                        catch
                        {
                            dr[1] = dt.Rows[index_row][1].ToString();
                        }
                        dr[2] = dt.Rows[index_row][2].ToString() == "" ? new DateTime?().GetValueOrDefault() : DateTime.Parse(dt.Rows[index_row][2].ToString());
                        switch (dt.Rows[index_row][3].ToString())
                        {
                            case "643":
                                dr[3] = "RUB";
                                break;
                            case "840":
                                dr[3] = "USD";
                                break;
                            case "978":
                                dr[3] = "EUR";
                                break;
                            default:
                                dr[3] = dt.Rows[index_row][3].ToString();
                                break;
                        }
                        dr[4] = dt.Rows[index_row][4].ToString() == "" ? new float?().GetValueOrDefault() : float.Parse(dt.Rows[index_row][4].ToString());
                        switch (dt.Rows[index_row][5].ToString())
                        {
                            case "643":
                                dr[5] = "RUB";
                                break;
                            case "840":
                                dr[5] = "USD";
                                break;
                            case "978":
                                dr[5] = "EUR";
                                break;
                            default:
                                dr[5] = dt.Rows[index_row][5].ToString();
                                break;
                        }
                        dr[6] = dt.Rows[index_row][6].ToString() == "" ? new float?().GetValueOrDefault() : float.Parse(dt.Rows[index_row][6].ToString());

                        dt_result.Rows.Add(dr);
                        dt_result.AcceptChanges();
                        index_row++;
                    }
                }
            }
            return dt_result;
        }

        //НЕ ИСПОЛЬЗУЕТСЯ.Перегруженный метод, альтернативный вариан первого. Очень медлено работает для большого кол-ва данных
        public DataTable GetParseTransactionPassportExcel(string file_path)
        {
            Excel.Application oXl;
            Excel._Workbook oWB;
            Excel._Worksheet oSheet;
            Excel.Range oRng = null;

            oXl = new Excel.Application();

            oXl.Visible = false;
            //делаем временно неактивным документ
            oXl.Interactive = false;
            oXl.EnableEvents = false;

            oWB = oXl.Workbooks.Open(file_path);
            oSheet = (Excel._Worksheet)oXl.Sheets["sheet"];
            oRng = oSheet.UsedRange;

            DataTable dt_result = new DataTable();
            dt_result.Columns.Add("П/п", typeof(string));
            dt_result.Columns.Add("Номер счёта", typeof(string));
            dt_result.Columns.Add("Дата счёта", typeof(DateTime));
            dt_result.Columns.Add("код валюты", typeof(string));
            dt_result.Columns.Add("Сумма", typeof(float));

            for (int rnum = 1; rnum <= oRng.Rows.Count; rnum++)
            {
                if (Convert.ToString((oRng.Cells[rnum, 1] as Excel.Range).Value2) == "Раздел III. Сведения о подтверждающих документах")
                {
                    int index_row = rnum + 7;

                    while (((Convert.ToString((oRng.Cells[index_row, 1] as Excel.Range).Value2) != "Подраздел III.II Сведения о подтверждающих документах (справочно)") && (Convert.ToString((oRng.Cells[index_row, 1] as Excel.Range).Value2) != "")))
                    {
                        DataRow dr = dt_result.NewRow();

                        dr[0] = (oRng.Cells[index_row, 1] as Excel.Range).Value2;
                        dr[1] = (oRng.Cells[index_row, 2] as Excel.Range).Value2;
                        dr[2] = (oRng.Cells[index_row, 11] as Excel.Range).Value2 == null ? DBNull.Value : (oRng.Cells[index_row, 11] as Excel.Range).Value2;

                        string currency = Convert.ToString((oRng.Cells[index_row, 20] as Excel.Range).Value2);
                        switch (currency)
                        {
                            case "643":
                                dr[3] = "RUB";
                                break;
                            case "840":
                                dr[3] = "USD";
                                break;
                            case "978":
                                dr[3] = "EUR";
                                break;
                            default:
                                dr[3] = (oRng.Cells[index_row, 3] as Excel.Range).Value2;
                                break;
                        }
                        dr[4] = Convert.ToSingle((oRng.Cells[index_row, 28] as Excel.Range).Value2);

                        dt_result.Rows.Add(dr);
                        dt_result.AcceptChanges();
                        index_row++;
                    }
                }
            }

            oXl.Quit();
            //Отсоединяемся от Excel
            releaseObject(oRng);
            releaseObject(oXl);

            return dt_result;
        }

        #region ОСВОБОЖДАЕМ РЕСУРЫ (ЗАКРЫВАЕМ eXCEL)
        public void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show(ex.ToString(), "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                GC.Collect();
            }
        }
        #endregion

        //ТЕСТ, НЕ ИСПОЛЬЗУЕТСЯ Обрабатываем excel файл паспорта сделки и возвращаем DataTable c данными ВБК "Раздел III. Сведения о подтверждающих документах"
        public DataTable ImportExceltoDatatableTest(string file_path, string sheet_name)
        {
            string sqlquery = string.Format("Select [F1],[F2],[F11],[F20],[F28],[F46],[F54] From [{0}$]", sheet_name);

            DataSet ds = new DataSet();

            string constring = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + file_path + "';Extended Properties=\"Excel 12.0;HDR=NO;IMEX=1\"";

            OleDbConnection con = new OleDbConnection(constring);
            OleDbDataAdapter da = new OleDbDataAdapter(sqlquery, con);
            da.Fill(ds);

            DataTable dt = ds.Tables[0];
            DataTable dt_result = new DataTable();

            dt_result.Columns.Add("П/п", typeof(string));
            dt_result.Columns.Add("Номер счёта", typeof(string));
            dt_result.Columns.Add("Дата счёта", typeof(DateTime));
            dt_result.Columns.Add("Код валюты, Платежа", typeof(string));
            dt_result.Columns.Add("Сумма, Платежа", typeof(float));
            dt_result.Columns.Add("Код валюты, Контракта", typeof(string));
            dt_result.Columns.Add("Сумма, Контракта", typeof(float));

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].ToString() == "Раздел III. Сведения о подтверждающих документах")
                {
                    int index_row = dt.Rows.IndexOf(dt.Rows[i]) + 7;

                    while ((dt.Rows[index_row][0].ToString() != "Подраздел III.II Сведения о подтверждающих документах (справочно)") && (dt.Rows[index_row][0].ToString() != ""))
                    {
                        DataRow dr = dt_result.NewRow();

                        dr[0] = dt.Rows[index_row][0].ToString();
                        try
                        {
                            dr[1] = Convert.ToDouble(dt.Rows[index_row][1].ToString());
                        }
                        catch
                        {
                            dr[1] = dt.Rows[index_row][1].ToString();
                        }
                        dr[2] = dt.Rows[index_row][2].ToString() == "" ? new DateTime?().GetValueOrDefault() : DateTime.Parse(dt.Rows[index_row][2].ToString());
                        switch (dt.Rows[index_row][3].ToString())
                        {
                            case "643":
                                dr[3] = "RUB";
                                break;
                            case "840":
                                dr[3] = "USD";
                                break;
                            case "978":
                                dr[3] = "EUR";
                                break;
                            default:
                                dr[3] = dt.Rows[index_row][3].ToString();
                                break;
                        }
                        dr[4] = dt.Rows[index_row][4].ToString() == "" ? new float?().GetValueOrDefault() : float.Parse(dt.Rows[index_row][4].ToString());
                        switch (dt.Rows[index_row][5].ToString())
                        {
                            case "643":
                                dr[3] = "RUB";
                                break;
                            case "840":
                                dr[3] = "USD";
                                break;
                            case "978":
                                dr[3] = "EUR";
                                break;
                            default:
                                dr[5] = dt.Rows[index_row][5].ToString();
                                break;
                        }
                        dr[6] = dt.Rows[index_row][6].ToString() == "" ? new float?().GetValueOrDefault() : float.Parse(dt.Rows[index_row][6].ToString());

                        dt_result.Rows.Add(dr);
                        dt_result.AcceptChanges();
                        index_row ++;
                    }
                }
            }
            return dt_result;
        }

        ///// <summary>
        ///// Импорт данных из Excel в DataGrigView на форму
        ///// </summary>
        ///// <param name="file_path">Путь к Excel файлу</param>
        ///// <param name="sheet_name">Название Excel Листа</param>
        ///// <returns></returns>
        //public DataTable ImportExceltoDatatable(string file_path, string sheet_name)
        //{
        //    DataSet ds = new DataSet();
        //    string constring = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + file_path + "';Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1\"";
        //    OleDbConnection con = new OleDbConnection(constring);
        //    con.Open();

        //    DataTable dtSheet = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

        //    List<string> listSheet = new List<string>();

        //    foreach (DataRow drSheet in dtSheet.Rows)
        //    {
        //        if (drSheet["TABLE_NAME"].ToString().Contains("$"))//checks whether row contains '_xlnm#_FilterDatabase' or sheet name(i.e. sheet name always ends with $ sign)
        //        {
        //            listSheet.Add(drSheet["TABLE_NAME"].ToString());
        //        }
        //    }


        //    foreach (var list in listSheet)
        //    {
        //        if (list.ToString().Contains(sheet_name + "$")==true)
        //        {
        //            sheet_name = list.ToString();
        //            break;
        //        }
        //    }

        //    string sqlquery = "";
        //    switch (sheet_name)
        //    {
        //        case "NonResident":
        //            sqlquery = string.Format("Select [Account], [Name 1], [Reference], [Assignment], [Type], [Doc# Date], [Amount in DC], [Curr#], [Flag] From [{0}] Where type='DV' and Flag in ('MR','NR') Order by [Doc# Date]", sheet_name);
        //            break;
        //        case "trade":
        //            sqlquery = string.Format("Select * From [{0}$] where len([Номер Паспорта сделки])=22 Order by 5,3", sheet_name);
        //            break;
        //        default:
        //            sqlquery = string.Format("Select * From [{0}]", sheet_name);
        //            break;
        //    }

        //    OleDbDataAdapter da = new OleDbDataAdapter(sqlquery, con);
        //    da.Fill(ds);
        //    DataTable dt = ds.Tables[0];

        //    return dt;
        //}
    }
}
