﻿using CurrencyControl.BL;
using System;
using System.Data;
using System.Deployment.Application;
using System.Windows.Forms;

namespace CurrencyControl
{
    public interface IMainForm
    {
        string FilePathNonResident { get; set; }
        string FilePathSaldo { get; set; }
        string PathStatementBankControl { get; set; }
        string FileStatementBankControl { get; set; }
        DataGridView DataGridViewDataSource { get; set; }

        event EventHandler FileSelectNonResidentClick;
        event EventHandler FileCheckNonResidentClick;

        event EventHandler FileSelectSaldoClick;
        event EventHandler FileCheckSaldoClick;

        event EventHandler FilePathStatementBankControlClick;
        event EventHandler FileSelectStatementBankControlClick;
        event EventHandler FileCheckStatementBankControlClick;

        event EventHandler PrepareClick;
        event EventHandler CurrencyControlReportClick;
        event EventHandler DefaultSettingsClick;
    }

    public partial class MainForm : Form, IMainForm
    {
        public MainForm()
        {
            InitializeComponent();

            butSelectFileNonResident.Click += ButSelectFileNonResident_Click;
            butFileCheckNonResident.Click += ButFileCheckNonResident_Click;

            butSelectFileSaldo.Click += ButSelectFileSaldo_Click;
            butFileCheckSaldo.Click += ButFileCheckSaldo_Click;
   
            butSelectPathStatementBankControl.Click += ButSelectPathStatementBankControl_Click;
            butSelectFileStatementBankControl.Click += ButSelectFileStatementBankControl_Click;
            butFileCheckStatementBankControl.Click += ButFileCheckStatementBankControl_Click;

            butPrepare.Click += ButPrepare_Click;
            butCurrencyControlReport.Click += ButCurrencyControlReport_Click;

            tsDefaultSettings.Click += TsDefaultSettings_Click;
        }

        private void ButSelectFileStatementBankControl_Click(object sender, EventArgs e)
        {
            FileSelectStatementBankControlClick?.Invoke(this, EventArgs.Empty);
        }

        #region IMainForm

        public string FilePathNonResident
        {
            get { return tbFilePathNonRasident.Text; }
            set { tbFilePathNonRasident.Text = value; }

        }

        public string FilePathSaldo
        {
            get { return tbFilePathSaldo.Text; }
            set { tbFilePathSaldo.Text = value; }
        }

        public string PathStatementBankControl
        {
            get { return tbPathStatementBankControl.Text; }
            set { tbPathStatementBankControl.Text = value; }
        }

        public string FileStatementBankControl
        {
            get { return tbFileStatementBankControl.Text; }
            set { tbFileStatementBankControl.Text = value; }
        }

        public DataGridView DataGridViewDataSource
        {
            get { return dgvDataSource; }
            set { dgvDataSource.DataSource = value; }
        }

        public event EventHandler FileSelectNonResidentClick;
        public event EventHandler FileCheckNonResidentClick;

        public event EventHandler FileSelectSaldoClick;
        public event EventHandler FileCheckSaldoClick;

        public event EventHandler FilePathStatementBankControlClick;
        public event EventHandler FileSelectStatementBankControlClick;
        public event EventHandler FileCheckStatementBankControlClick;

        public event EventHandler PrepareClick;
        public event EventHandler CurrencyControlReportClick;
        public event EventHandler DefaultSettingsClick;

        #endregion

        private void MainForm_Load(object sender, EventArgs e)
        {
            // версия компиляции
            Version ver = ApplicationDeployment.CurrentDeployment.CurrentVersion;
            lblVersion.Text = "ver. " + ver.ToString();
        }

        #region Проброс событий
        //Выбрать файл NonResident
        private void ButSelectFileNonResident_Click(object sender, EventArgs e)
        {
            FileSelectNonResidentClick?.Invoke(this, EventArgs.Empty);
        }
        
        //Проверить файл NonResident
        private void ButFileCheckNonResident_Click(object sender, EventArgs e)
        {
            FileCheckNonResidentClick?.Invoke(this, EventArgs.Empty);
        }

        //Выбрать файл Saldo
        private void ButSelectFileSaldo_Click(object sender, EventArgs e)
        {
            FileSelectSaldoClick?.Invoke(this, EventArgs.Empty);
        }

        //Проверить файл Saldo
        private void ButFileCheckSaldo_Click(object sender, EventArgs e)
        {
            //Старый способ
            //if (FileCheckSaldoClick != null) FileCheckSaldoClick(this, EventArgs.Empty);
            //Новый способ
            FileCheckSaldoClick?.Invoke(this, EventArgs.Empty);
        }

        //Подготовка данных, присоединение номера ПС
        private void ButPrepare_Click(object sender, EventArgs e)
        {
            PrepareClick?.Invoke(this, EventArgs.Empty);
        }

        //Выбрать путь к ВБК файлам
        private void ButSelectPathStatementBankControl_Click(object sender, EventArgs e)
        {
            FilePathStatementBankControlClick?.Invoke(this, EventArgs.Empty);
        }

        //Проверка файла ВБК
        private void ButFileCheckStatementBankControl_Click(object sender, EventArgs e)
        {
            FileCheckStatementBankControlClick?.Invoke(this, EventArgs.Empty);
        }

        //Отчет по валютному контролю
        private void ButCurrencyControlReport_Click(object sender, EventArgs e)
        {
            CurrencyControlReportClick?.Invoke(this, EventArgs.Empty);
        }

        //Сброс пользовательских настроек на настройки по умолчанию
        private void TsDefaultSettings_Click(object sender, EventArgs e)
        {
            DefaultSettingsClick?.Invoke(this, EventArgs.Empty);
        }

        #endregion
    }
}
