﻿namespace CurrencyControl
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.dtpStartDatePeriod = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDatePeriod = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.tbFilePathNonRasident = new System.Windows.Forms.TextBox();
            this.tbFilePathSaldo = new System.Windows.Forms.TextBox();
            this.tbPathStatementBankControl = new System.Windows.Forms.TextBox();
            this.dgvDataSource = new System.Windows.Forms.DataGridView();
            this.lblNonResident = new System.Windows.Forms.Label();
            this.lblSaldo = new System.Windows.Forms.Label();
            this.lblPathStatementBankControl = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.tsSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.tsDefaultSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.lblVersion = new System.Windows.Forms.Label();
            this.butFileCheckStatementBankControl = new System.Windows.Forms.Button();
            this.butPrepare = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.butCurrencyControlReport = new System.Windows.Forms.Button();
            this.butFileCheckNonResident = new System.Windows.Forms.Button();
            this.butFileCheckSaldo = new System.Windows.Forms.Button();
            this.butSelectPathStatementBankControl = new System.Windows.Forms.Button();
            this.butSelectFileSaldo = new System.Windows.Forms.Button();
            this.butSelectFileNonResident = new System.Windows.Forms.Button();
            this.tbFileStatementBankControl = new System.Windows.Forms.TextBox();
            this.lblFileStatementBankControl = new System.Windows.Forms.Label();
            this.butSelectFileStatementBankControl = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDataSource)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtpStartDatePeriod
            // 
            this.dtpStartDatePeriod.Location = new System.Drawing.Point(122, 35);
            this.dtpStartDatePeriod.Name = "dtpStartDatePeriod";
            this.dtpStartDatePeriod.Size = new System.Drawing.Size(200, 20);
            this.dtpStartDatePeriod.TabIndex = 0;
            // 
            // dtpEndDatePeriod
            // 
            this.dtpEndDatePeriod.Location = new System.Drawing.Point(328, 35);
            this.dtpEndDatePeriod.Name = "dtpEndDatePeriod";
            this.dtpEndDatePeriod.Size = new System.Drawing.Size(200, 20);
            this.dtpEndDatePeriod.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Период даты счета";
            // 
            // tbFilePathNonRasident
            // 
            this.tbFilePathNonRasident.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilePathNonRasident.Location = new System.Drawing.Point(122, 79);
            this.tbFilePathNonRasident.Name = "tbFilePathNonRasident";
            this.tbFilePathNonRasident.ReadOnly = true;
            this.tbFilePathNonRasident.Size = new System.Drawing.Size(599, 20);
            this.tbFilePathNonRasident.TabIndex = 5;
            // 
            // tbFilePathSaldo
            // 
            this.tbFilePathSaldo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilePathSaldo.Location = new System.Drawing.Point(122, 122);
            this.tbFilePathSaldo.Name = "tbFilePathSaldo";
            this.tbFilePathSaldo.ReadOnly = true;
            this.tbFilePathSaldo.Size = new System.Drawing.Size(599, 20);
            this.tbFilePathSaldo.TabIndex = 6;
            // 
            // tbPathStatementBankControl
            // 
            this.tbPathStatementBankControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPathStatementBankControl.Location = new System.Drawing.Point(122, 208);
            this.tbPathStatementBankControl.Name = "tbPathStatementBankControl";
            this.tbPathStatementBankControl.ReadOnly = true;
            this.tbPathStatementBankControl.Size = new System.Drawing.Size(599, 20);
            this.tbPathStatementBankControl.TabIndex = 7;
            // 
            // dgvDataSource
            // 
            this.dgvDataSource.AllowUserToAddRows = false;
            this.dgvDataSource.AllowUserToDeleteRows = false;
            this.dgvDataSource.AllowUserToOrderColumns = true;
            this.dgvDataSource.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDataSource.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDataSource.Location = new System.Drawing.Point(0, 241);
            this.dgvDataSource.Name = "dgvDataSource";
            this.dgvDataSource.ReadOnly = true;
            this.dgvDataSource.Size = new System.Drawing.Size(949, 374);
            this.dgvDataSource.TabIndex = 13;
            // 
            // lblNonResident
            // 
            this.lblNonResident.AutoSize = true;
            this.lblNonResident.Location = new System.Drawing.Point(12, 82);
            this.lblNonResident.Name = "lblNonResident";
            this.lblNonResident.Size = new System.Drawing.Size(69, 13);
            this.lblNonResident.TabIndex = 14;
            this.lblNonResident.Text = "NonResident";
            // 
            // lblSaldo
            // 
            this.lblSaldo.AutoSize = true;
            this.lblSaldo.Location = new System.Drawing.Point(12, 125);
            this.lblSaldo.Name = "lblSaldo";
            this.lblSaldo.Size = new System.Drawing.Size(77, 13);
            this.lblSaldo.TabIndex = 15;
            this.lblSaldo.Text = "Сальдо по ПС";
            // 
            // lblPathStatementBankControl
            // 
            this.lblPathStatementBankControl.AutoSize = true;
            this.lblPathStatementBankControl.Location = new System.Drawing.Point(12, 211);
            this.lblPathStatementBankControl.Name = "lblPathStatementBankControl";
            this.lblPathStatementBankControl.Size = new System.Drawing.Size(72, 13);
            this.lblPathStatementBankControl.TabIndex = 16;
            this.lblPathStatementBankControl.Text = "Папка с ВБК";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(949, 24);
            this.menuStrip1.TabIndex = 17;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsMenu
            // 
            this.tsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsSettings});
            this.tsMenu.Name = "tsMenu";
            this.tsMenu.Size = new System.Drawing.Size(53, 20);
            this.tsMenu.Text = "Меню";
            // 
            // tsSettings
            // 
            this.tsSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsDefaultSettings});
            this.tsSettings.Name = "tsSettings";
            this.tsSettings.Size = new System.Drawing.Size(134, 22);
            this.tsSettings.Text = "Настройки";
            // 
            // tsDefaultSettings
            // 
            this.tsDefaultSettings.Name = "tsDefaultSettings";
            this.tsDefaultSettings.Size = new System.Drawing.Size(224, 22);
            this.tsDefaultSettings.Text = "Параметры по умолчанию";
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(875, 11);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(61, 13);
            this.lblVersion.TabIndex = 18;
            this.lblVersion.Text = "ver. 0.0.0.0";
            // 
            // butFileCheckStatementBankControl
            // 
            this.butFileCheckStatementBankControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butFileCheckStatementBankControl.Image = global::CurrencyControl.Properties.Resources.binoculars_Check;
            this.butFileCheckStatementBankControl.Location = new System.Drawing.Point(768, 157);
            this.butFileCheckStatementBankControl.Name = "butFileCheckStatementBankControl";
            this.butFileCheckStatementBankControl.Size = new System.Drawing.Size(81, 35);
            this.butFileCheckStatementBankControl.TabIndex = 20;
            this.toolTip1.SetToolTip(this.butFileCheckStatementBankControl, "Посмотреть файл ВБК. Файл должент содержать лист sheet");
            this.butFileCheckStatementBankControl.UseVisualStyleBackColor = true;
            // 
            // butPrepare
            // 
            this.butPrepare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butPrepare.Location = new System.Drawing.Point(768, 200);
            this.butPrepare.Name = "butPrepare";
            this.butPrepare.Size = new System.Drawing.Size(81, 35);
            this.butPrepare.TabIndex = 21;
            this.butPrepare.Text = "Подготовка";
            this.toolTip1.SetToolTip(this.butPrepare, "Посмотреть у каких счетов файла NonResident отсутствует номер ПС");
            this.butPrepare.UseVisualStyleBackColor = true;
            // 
            // butCurrencyControlReport
            // 
            this.butCurrencyControlReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butCurrencyControlReport.Image = global::CurrencyControl.Properties.Resources.list__2_;
            this.butCurrencyControlReport.Location = new System.Drawing.Point(855, 157);
            this.butCurrencyControlReport.Name = "butCurrencyControlReport";
            this.butCurrencyControlReport.Size = new System.Drawing.Size(81, 78);
            this.butCurrencyControlReport.TabIndex = 19;
            this.toolTip1.SetToolTip(this.butCurrencyControlReport, "Сформировать отчёт по Валютному контролю");
            this.butCurrencyControlReport.UseVisualStyleBackColor = true;
            // 
            // butFileCheckNonResident
            // 
            this.butFileCheckNonResident.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butFileCheckNonResident.Image = global::CurrencyControl.Properties.Resources.binoculars_Check;
            this.butFileCheckNonResident.Location = new System.Drawing.Point(768, 71);
            this.butFileCheckNonResident.Name = "butFileCheckNonResident";
            this.butFileCheckNonResident.Size = new System.Drawing.Size(81, 35);
            this.butFileCheckNonResident.TabIndex = 12;
            this.toolTip1.SetToolTip(this.butFileCheckNonResident, "Посмотреть файл NonResident. Файл должент содержать лист NonResident");
            this.butFileCheckNonResident.UseVisualStyleBackColor = true;
            // 
            // butFileCheckSaldo
            // 
            this.butFileCheckSaldo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butFileCheckSaldo.Image = global::CurrencyControl.Properties.Resources.binoculars_Check;
            this.butFileCheckSaldo.Location = new System.Drawing.Point(768, 114);
            this.butFileCheckSaldo.Name = "butFileCheckSaldo";
            this.butFileCheckSaldo.Size = new System.Drawing.Size(81, 35);
            this.butFileCheckSaldo.TabIndex = 11;
            this.toolTip1.SetToolTip(this.butFileCheckSaldo, "Посмотреть файл Сальдо по ПС. Файл должен содержать лист trade");
            this.butFileCheckSaldo.UseVisualStyleBackColor = true;
            // 
            // butSelectPathStatementBankControl
            // 
            this.butSelectPathStatementBankControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectPathStatementBankControl.Image = global::CurrencyControl.Properties.Resources.edit;
            this.butSelectPathStatementBankControl.Location = new System.Drawing.Point(727, 200);
            this.butSelectPathStatementBankControl.Name = "butSelectPathStatementBankControl";
            this.butSelectPathStatementBankControl.Size = new System.Drawing.Size(35, 35);
            this.butSelectPathStatementBankControl.TabIndex = 10;
            this.toolTip1.SetToolTip(this.butSelectPathStatementBankControl, "Выбрать папку с файлами ВБК");
            this.butSelectPathStatementBankControl.UseVisualStyleBackColor = true;
            // 
            // butSelectFileSaldo
            // 
            this.butSelectFileSaldo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectFileSaldo.Image = global::CurrencyControl.Properties.Resources.edit;
            this.butSelectFileSaldo.Location = new System.Drawing.Point(727, 114);
            this.butSelectFileSaldo.Name = "butSelectFileSaldo";
            this.butSelectFileSaldo.Size = new System.Drawing.Size(35, 35);
            this.butSelectFileSaldo.TabIndex = 9;
            this.toolTip1.SetToolTip(this.butSelectFileSaldo, "Выбрать файл Сальдо по ПC");
            this.butSelectFileSaldo.UseVisualStyleBackColor = true;
            // 
            // butSelectFileNonResident
            // 
            this.butSelectFileNonResident.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectFileNonResident.Image = global::CurrencyControl.Properties.Resources.edit;
            this.butSelectFileNonResident.Location = new System.Drawing.Point(727, 71);
            this.butSelectFileNonResident.Name = "butSelectFileNonResident";
            this.butSelectFileNonResident.Size = new System.Drawing.Size(35, 35);
            this.butSelectFileNonResident.TabIndex = 8;
            this.toolTip1.SetToolTip(this.butSelectFileNonResident, "Выбрать файл NonResident");
            this.butSelectFileNonResident.UseVisualStyleBackColor = true;
            // 
            // tbFileStatementBankControl
            // 
            this.tbFileStatementBankControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFileStatementBankControl.Location = new System.Drawing.Point(122, 165);
            this.tbFileStatementBankControl.Name = "tbFileStatementBankControl";
            this.tbFileStatementBankControl.ReadOnly = true;
            this.tbFileStatementBankControl.Size = new System.Drawing.Size(599, 20);
            this.tbFileStatementBankControl.TabIndex = 22;
            // 
            // lblFileStatementBankControl
            // 
            this.lblFileStatementBankControl.AutoSize = true;
            this.lblFileStatementBankControl.Location = new System.Drawing.Point(12, 168);
            this.lblFileStatementBankControl.Name = "lblFileStatementBankControl";
            this.lblFileStatementBankControl.Size = new System.Drawing.Size(28, 13);
            this.lblFileStatementBankControl.TabIndex = 23;
            this.lblFileStatementBankControl.Text = "ВБК";
            // 
            // butSelectFileStatementBankControl
            // 
            this.butSelectFileStatementBankControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.butSelectFileStatementBankControl.Image = global::CurrencyControl.Properties.Resources.edit;
            this.butSelectFileStatementBankControl.Location = new System.Drawing.Point(727, 157);
            this.butSelectFileStatementBankControl.Name = "butSelectFileStatementBankControl";
            this.butSelectFileStatementBankControl.Size = new System.Drawing.Size(35, 35);
            this.butSelectFileStatementBankControl.TabIndex = 24;
            this.toolTip1.SetToolTip(this.butSelectFileStatementBankControl, "Выбрать файл ВБК");
            this.butSelectFileStatementBankControl.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 615);
            this.Controls.Add(this.butSelectFileStatementBankControl);
            this.Controls.Add(this.lblFileStatementBankControl);
            this.Controls.Add(this.tbFileStatementBankControl);
            this.Controls.Add(this.butPrepare);
            this.Controls.Add(this.butFileCheckStatementBankControl);
            this.Controls.Add(this.butCurrencyControlReport);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lblPathStatementBankControl);
            this.Controls.Add(this.lblSaldo);
            this.Controls.Add(this.lblNonResident);
            this.Controls.Add(this.dgvDataSource);
            this.Controls.Add(this.butFileCheckNonResident);
            this.Controls.Add(this.butFileCheckSaldo);
            this.Controls.Add(this.butSelectPathStatementBankControl);
            this.Controls.Add(this.butSelectFileSaldo);
            this.Controls.Add(this.tbPathStatementBankControl);
            this.Controls.Add(this.tbFilePathSaldo);
            this.Controls.Add(this.tbFilePathNonRasident);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpEndDatePeriod);
            this.Controls.Add(this.dtpStartDatePeriod);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.butSelectFileNonResident);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Валютный контроль";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDataSource)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpStartDatePeriod;
        private System.Windows.Forms.DateTimePicker dtpEndDatePeriod;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbFilePathNonRasident;
        private System.Windows.Forms.TextBox tbFilePathSaldo;
        private System.Windows.Forms.TextBox tbPathStatementBankControl;
        private System.Windows.Forms.Button butSelectFileNonResident;
        private System.Windows.Forms.Button butSelectFileSaldo;
        private System.Windows.Forms.Button butSelectPathStatementBankControl;
        private System.Windows.Forms.Button butFileCheckSaldo;
        private System.Windows.Forms.Button butFileCheckNonResident;
        private System.Windows.Forms.DataGridView dgvDataSource;
        private System.Windows.Forms.Label lblNonResident;
        private System.Windows.Forms.Label lblSaldo;
        private System.Windows.Forms.Label lblPathStatementBankControl;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsMenu;
        private System.Windows.Forms.ToolStripMenuItem tsSettings;
        private System.Windows.Forms.ToolStripMenuItem tsDefaultSettings;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Button butCurrencyControlReport;
        private System.Windows.Forms.Button butFileCheckStatementBankControl;
        private System.Windows.Forms.Button butPrepare;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox tbFileStatementBankControl;
        private System.Windows.Forms.Label lblFileStatementBankControl;
        private System.Windows.Forms.Button butSelectFileStatementBankControl;
    }
}

