﻿using CurrencyControl.BL;
using System;
using System.Data;
using System.Deployment.Application;
using System.Windows.Forms;

namespace CurrencyControl
{
    public class MainPresenter
    {
        private readonly IMainForm _view;
        private readonly IFileManager _manager;
        private readonly IMessageService _messageService;

        public MainPresenter(IMainForm view, IFileManager manager, IMessageService service)
        {
            _view = view;
            _manager = manager;
            _messageService = service;

            _view.FilePathNonResident = Properties.Settings.Default.FileNonResident;
            _view.FilePathSaldo = Properties.Settings.Default.FileSaldo;
            _view.FileStatementBankControl = Properties.Settings.Default.FileStatementBankControl;
            _view.PathStatementBankControl = Properties.Settings.Default.PathStatementBankControl;

            _view.FileSelectNonResidentClick += _view_FileSelectNonResidentClick;
            _view.FileCheckNonResidentClick += _view_FileCheckNonResidentClick;

            _view.FileSelectSaldoClick += _view_FileSelectSaldoClick;
            _view.FileCheckSaldoClick += _view_FileCheckSaldoClick;

            _view.FileSelectStatementBankControlClick += _view_FileSelectStatementBankControlClick;
            _view.FileCheckStatementBankControlClick += _view_FileCheckStatementBankControlClick;
            _view.FilePathStatementBankControlClick += _view_FilePathStatementBankControlClick;

            _view.PrepareClick += _view_PrepareClick;
            _view.CurrencyControlReportClick += _view_CurrencyControlReportClick;
            _view.DefaultSettingsClick += _view_DefaultSettingsClick;
        }

        private void _view_DefaultSettingsClick(object sender, EventArgs e)
        {
            switch (MessageBox.Show("Сделать настройки по умолчанию?", "Предупреждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                case DialogResult.Yes:
                    Properties.Settings.Default.FileNonResident = Properties.Settings.Default.DefaultFileNonResident;
                    _view.FilePathNonResident = Properties.Settings.Default.DefaultFileNonResident;

                    Properties.Settings.Default.FileSaldo = Properties.Settings.Default.DefaultFileSaldo;
                    _view.FilePathSaldo = Properties.Settings.Default.DefaultFileSaldo;

                    Properties.Settings.Default.FileStatementBankControl = Properties.Settings.Default.DefaultFileStatementBankControl;
                    _view.FileStatementBankControl = Properties.Settings.Default.DefaultFileStatementBankControl;

                    Properties.Settings.Default.PathStatementBankControl = Properties.Settings.Default.DefaultPathStatementBankControl;
                    _view.PathStatementBankControl = Properties.Settings.Default.DefaultPathStatementBankControl;

                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void _view_CurrencyControlReportClick(object sender, EventArgs e)
        {
            try
            {
                bool isExist_1 = _manager.IsExist(_view.FilePathNonResident);
                bool isExist_2 = _manager.IsExist(_view.FilePathSaldo);
                bool isExistDirectrory = _manager.isExistDirectory(_view.PathStatementBankControl);

                if (!isExist_1 || !isExist_2)
                {
                    _messageService.ShowExclamation("Выбранный файл не существует");
                    return;
                }

                if (!isExistDirectrory)
                {
                    _messageService.ShowExclamation("Выбранный путь не существует");
                    return;
                }

                DataTable dt_nonresident = _manager.ImportExceltoDatatable(_view.FilePathNonResident, "NonResident");
                DataTable dt_saldo = _manager.ImportExceltoDatatable(_view.FilePathSaldo, "trade");
                DataTable dt_prepare = _manager.GetPrepareData(dt_nonresident, dt_saldo);
                DataTable dt_cur_control = _manager.GetTransactionPassportDetect(dt_prepare, _view.PathStatementBankControl);

                _view.DataGridViewDataSource.DataSource = dt_cur_control;
                _view.DataGridViewDataSource.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            catch (Exception ex)
            {
                _messageService.ShowError(ex.Message);
            }
        }

        private void _view_FileSelectStatementBankControlClick(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "File Excel(*.xls;*.xlsx)|*.xls;*.xlsx|All files(*.*)|*.*";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                _view.FileStatementBankControl = dlg.FileName;
                Properties.Settings.Default.FileStatementBankControl = dlg.FileName;
                Properties.Settings.Default.Save();
            }
        }

        private void _view_FileCheckStatementBankControlClick(object sender, EventArgs e)
        {
            try
            {
                bool isExist = _manager.IsExist(_view.FileStatementBankControl);
                if (!isExist)
                {
                    _messageService.ShowExclamation("Выбранный файл не существует");
                    return;
                }
                _view.DataGridViewDataSource.DataSource = _manager.GetParseTransactionPassportExcel(_view.FileStatementBankControl, "sheet");
                _view.DataGridViewDataSource.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            catch (Exception ex)
            {
                _messageService.ShowError(ex.Message);
            }
        }

        private void _view_FileSelectNonResidentClick(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "File Excel(*.xls;*.xlsx)|*.xls;*.xlsx|All files(*.*)|*.*";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                _view.FilePathNonResident= dlg.FileName;
                Properties.Settings.Default.FileNonResident = dlg.FileName;
                Properties.Settings.Default.Save();
            }
        }

        private void _view_FileSelectSaldoClick(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "File Excel(*.xls;*.xlsx)|*.xls;*.xlsx|All files(*.*)|*.*";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                _view.FilePathSaldo = dlg.FileName;
                Properties.Settings.Default.FileSaldo = dlg.FileName;
                Properties.Settings.Default.Save();
            }
        }

        private void _view_FilePathStatementBankControlClick(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                _view.PathStatementBankControl = fbd.SelectedPath;
                Properties.Settings.Default.PathStatementBankControl = fbd.SelectedPath;
                Properties.Settings.Default.Save();
            }
        }

        private void _view_FileCheckNonResidentClick(object sender, EventArgs e)
        {
            try
            {
                bool isExist = _manager.IsExist(_view.FilePathNonResident);
                if (!isExist)
                {
                    _messageService.ShowExclamation("Выбранный файл не существует");
                    return;
                }

                _view.DataGridViewDataSource.DataSource = _manager.ImportExceltoDatatable(_view.FilePathNonResident, "NonResident");
                _view.DataGridViewDataSource.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            catch (Exception ex)
            {
                _messageService.ShowError(ex.Message);
            }
        }

        private void _view_FileCheckSaldoClick(object sender, EventArgs e)
        {
            try
            {
                bool isExist = _manager.IsExist(_view.FilePathSaldo);
                if (!isExist)
                {
                    _messageService.ShowExclamation("Выбранный файл не существует");
                    return;
                }

                _view.DataGridViewDataSource.DataSource = _manager.ImportExceltoDatatable(_view.FilePathSaldo, "trade");
                _view.DataGridViewDataSource.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            catch (Exception ex)
            {
                _messageService.ShowError(ex.Message);
            }
        }

        private void _view_PrepareClick(object sender, EventArgs e)
        {
            try
            {
                bool isExist_1 = _manager.IsExist(_view.FilePathNonResident);
                bool isExist_2 = _manager.IsExist(_view.FilePathSaldo);
                if (!isExist_1 || !isExist_2)
                {
                    _messageService.ShowExclamation("Выбранный файл не существует");
                    return;
                }

                DataTable dt_nonresident = _manager.ImportExceltoDatatable(_view.FilePathNonResident, "NonResident");
                DataTable dt_saldo = _manager.ImportExceltoDatatable(_view.FilePathSaldo, "trade");

                DataTable dt_prepare = _manager.GetPrepareData(dt_nonresident, dt_saldo);

                _view.DataGridViewDataSource.DataSource = dt_prepare;
                _view.DataGridViewDataSource.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            catch (Exception ex)
            {
                _messageService.ShowError(ex.Message);
            }
        }
    }
}
